// SPDX-FileCopyrightText: 2002-2024 Rice University
// SPDX-FileCopyrightText: 2024 Contributors to the HPCToolkit Project
//
// SPDX-License-Identifier: BSD-3-Clause

// -*-Mode: C++;-*-

// Funnel default names through one place for consistency.

#ifndef __DICTIONARY_H__
#define __DICTIONARY_H__

#define GUARD_NAME  "<inline>"

#define UNKNOWN_FILE  "<unknown file>"
#define UNKNOWN_PROC  "<unknown procedure>"
#define UNKNOWN_LINK  "_unknown_procedure_"

#define UNKNOWN_LOAD_MODULE  "<unknown load module>"

#define PARTIAL_CALL_PATHS   "<partial call paths>"

#endif

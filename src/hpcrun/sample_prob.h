// SPDX-FileCopyrightText: 2002-2024 Rice University
// SPDX-FileCopyrightText: 2024 Contributors to the HPCToolkit Project
//
// SPDX-License-Identifier: BSD-3-Clause

// -*-Mode: C++;-*- // technically C99

#ifndef _HPCRUN_SAMPLE_PROB_
#define _HPCRUN_SAMPLE_PROB_

void hpcrun_sample_prob_init(void);
int  hpcrun_sample_prob_active(void);
void hpcrun_sample_prob_mesg(void);

#endif // _HPCRUN_SAMPLE_PROB_

// SPDX-FileCopyrightText: 2016-2024 Rice University
// SPDX-FileCopyrightText: 2024 Contributors to the HPCToolkit Project
//
// SPDX-License-Identifier: BSD-3-Clause

// fixme: add body for trampoline and trampoline end.
// this is just enough to let hpcrun link and run.

void hpcrun_trampoline(void) { }
void hpcrun_trampoline_end(void) { }

// SPDX-FileCopyrightText: 2002-2024 Rice University
// SPDX-FileCopyrightText: 2024 Contributors to the HPCToolkit Project
//
// SPDX-License-Identifier: BSD-3-Clause

// -*-Mode: C++;-*- // technically C99

#ifndef UNW_THROW_H
#define UNW_THROW_H

extern void hpcrun_unw_throw(void);
extern void hpcrun_unw_drop(void);

#endif // UNW_THROW_H
